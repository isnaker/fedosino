var gulp = require('gulp');
var runSequence = require('run-sequence');
var concat = require('gulp-concat');
var cleanCSS = require('gulp-clean-css');
var concatCss = require('gulp-concat-css');
var minify = require('gulp-minify');
var uncss = require('gulp-uncss');

gulp.task('default', function() {
    runSequence('css','js')
});

gulp.task('css', function() {
    return gulp.src('app/css/*.css')
        .pipe(concatCss("css/style.css"))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('dist/'));
});

gulp.task('js', function () {
    return gulp.src('app/js/*.js')
        .pipe(concat('scripts.js'))
        .pipe(minify({
            ext:{
                src:'-debug.js',
                min:'.js'
            },
            noSource: true
        }))
        .pipe(gulp.dest('dist/js/'));
});