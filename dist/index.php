<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Page Description">
    <meta name="author" content="Alex">
    <title>Page Title</title>
    <!-- Bootstrap -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<header id="header">
    <div class="container">
        <div class="row">

            <div class="col-md-4">
                <p class="h3 strongest mt-md mb-none">Удачный сервис</p>
                <p class="h6 mt-md">Комплекс услуг в вашем поселке</p>
            </div>
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <p class="h3 text-center lighter mt-lg"><span data-value="app_phone">8 (999) 956-32-21</span></p>
            </div>
        </div>
    </div>
</header>


    <div class="container-fluid" id="preview">
        <div class="row">
            <div  class="color-white">
                <div class="col-md-6 col-md-offset-3">
                    <div class="promo">
                        <h1 class="text-center strongest text-uppercase">
                            <span data-value="selected_service">земельные работы</span>
                        </h1>
                        <p class="description text-center">
                            работы по рытью котлованов под фундамент здания или под искусственный пруд. Они обычно осуществляются с помощью экскаватора и другого специализированного оборудования.
                        </p>

                        <p class="text-center h3 mt-xlg">Закажите прямо сейчас</p>
                        <button class="btn btn-primary btn-lg center-block mt-lg">Заказать обратный звонок</button>
                        <p class="text-center mt-lg">Или позвоните по телефону</p>
                        <p class="h3 text-center strongest mt-md ">8 (999) 956-32-21</p>
                    </div>
                </div>


            </div>
        </div>

        <div id="preview-left-stripe"></div>
        <div id="preview-right-stripe"></div>
    </div>

<div id="content">
    <div class="container color-white services mt-xlg">
        <div class="row">
            <div class="col-md-3">
                <ul class="list-unstyled services__list">
                    <li class="item current" data-target="earthwork">
                        <p>земельные работы</p>
                    </li>
                    <li class="item" data-target="bitovki">
                        <p>Бытовки</p>
                    </li>
                    <li class="item" data-target="toilets">
                        <p>Уличные туалеты</p>
                    </li>
                    <li class="item" data-target="burenie">
                        <p>Бурение Скважин</p>
                    </li>
                    <li class="item" data-target="septik">
                        <p>Переливные септики</p>
                    </li>
                    <li class="item" data-target="communication">
                        <p>Прокладка коммуникаций</p>
                    </li>
                    <li class="item" data-target="design">
                        <p>Планировка и ландшафтный дизайн</p>
                    </li>
                    <li class="item" data-target="urist">
                        <p>Юридическая поддержка</p>
                    </li>
                    <li class="item" data-target="master">
                        <p>Мастер на час</p>
                    </li>
                    <li class="item" data-target="bani">
                        <p>Бани и летние дома</p>
                    </li>
                    <li class="item" data-target="zabor">
                        <p>Строительство заборов</p>
                    </li>
                    <li class="item" data-target="viezd">
                        <p>Обустройство въездных групп</p>
                    </li>
                    <li class="item" data-target="electric"><p>Электрика</p></li>
                    <li class="item" data-target="santehnic"><p>Сантехника</p></li>
                    <li class="item" data-target="otdelka"><p>Отделка помещений</p></li>
                    <li class="item" data-target="mebel"><p>Садовая мебель на заказ</p></li>
                </ul>
            </div>
            <div class="col-md-9">
                <div class="service__content">
                    <div class="item" data-value="earthwork">
                        <h2 class="strongest">Земельные работы</h2>
                        <p>Комплекс строительных работ, включающий выемку (разработку) грунта, перемещение его и укладку
                            в определённое место (процесс укладки в ряде случаев сопровождается разравниванием и
                            уплотнением грунта). Земельные работы являются одним из важнейших элементов промышленного,
                            гидротехнического, транспортного, жилищно-гражданского строительства. Цель земельных работ —
                            создание инженерных сооружений из грунта (плотин, железных и автомобильных дорог,
                            каналов, траншей и т.д.), устройство оснований зданий и сооружений, воздвигаемых из др.
                            материалов, планировка территорий под застройку, а также удаление земляных масс для
                            вскрытия месторождений полезных ископаемых.  Земельные работы, связанные с добычей полезных ископаемых
                            открытым способом, относятся к горным работам (см. Вскрышные работы). Земляные сооружения
                            создаются путём выемок в грунте или возведением из него насыпей.
                            Выемка, отрываемая только для добычи грунта, называется резервом, а насыпь,
                            образованная при отсыпке излишнего грунта, — отвалом.
                        </p>
                        <table border="0" class="sectionBorder" style="width: 100%;" cellspacing="1" cellpadding="1">
                            <tbody>
                            <tr class="lineitemRowShaded">
                                <td colspan="3">
                                    <div>
                                        <h3>Цены на земляные работы в 2017 году</h3>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div><strong>Вид работ</strong></div>
                                </td>
                                <td>
                                    <div><strong>Ед.изм.</strong></div>
                                </td>
                                <td>
                                    <div class="text-center"><strong>Цена</strong></div>
                                </td>
                            </tr>
                            <tr>
                                <td>Разработка грунта вручную без вывозки грунта</td>
                                <td>
                                    <div align="center">м<sup>3</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 600</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Разработка грунта вручную с загрузкой и вывозкой грунта</td>
                                <td>
                                    <div align="center">м<sup>3</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 700</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Разработка грунта механизированным способом (экскаватор и тд.)</td>
                                <td>
                                    <div align="center">м<sup>3</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 100</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Разработка грунта механизированным способом (экскаватор и тд.) с погрузкой в самосвалы и вывозом</td>
                                <td>
                                    <div align="center">м<sup>3</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 300</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Обратная засыпка котлована</td>
                                <td>
                                    <div align="center">м<sup>3</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 300</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Планировка механизированным способом</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 100</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Устройство основания толщиной 150мм из щебня</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 120</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Устройство основания толщиной 150мм из ПГС</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 80</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Устройство газона (в стоимость входит верхний плодородный слой земли 100мм, планировка места будущего газона, посев травосмеси)</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 100</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Карчевка, удаление пней</td>
                                <td>
                                    <div align="center">шт</div>
                                </td>
                                <td>
                                    <div align="center">от 200</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Подготовка грунта (вскапывание, культивирование, удаление корней и корневищ сорняков)</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 80</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Валка деревьев с корнем (с распилом на полена 300мм)</td>
                                <td>
                                    <div align="center">шт</div>
                                </td>
                                <td>
                                    <div align="center">от 1200</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Выезд специалиста на объект для оценки объяма и стоимости работ</td>
                                <td>
                                    <div align="center">шт</div>
                                </td>
                                <td>
                                    <div align="center"><strong>БЕСПЛАТНО</strong></div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Устройство основания из песка с уплотнением толщиной 100мм</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 100</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Устройство основания из песка с уплотнением толщиной 200мм</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 200</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Устройство основания из песка с уплотнением толщиной 300мм</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 300</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Погрузка и вывоз строительного мусора</td>
                                <td>
                                    <div align="center">м<sup>3</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 400</div>
                                </td>
                            </tr>
                            <tr class="lineitemRowShaded">
                                <td>Устройство асфальтобетонного покрытия толщиной 50мм</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 450</div>
                                </td>
                            </tr>
                            <tr>
                                <td>Устройство асфальтобетонного покрытия толщиной 100мм</td>
                                <td>
                                    <div align="center">м<sup>2</sup></div>
                                </td>
                                <td>
                                    <div align="center">от 900</div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="item hidden" data-value="bitovki">
                        <h2 class="strongest">Бытовки</h2>
                        <p>Деревянные бытовки – один из видов временной постройки, который используется для
                            проживания рабочих на объектах, заменяет летний дом на дачном участке. Главные
                            преимущества такой конструкции – небольшой вес, экономия строительных материалов,
                            сравнительно небольшая стоимость и простота транспортировки и эксплуатации.</p>

                        <p>Данный тип бытовок условно можно разделить на два вида – строительные и дачные.
                        Разница между видами заключается в конструктивных особенностях. Так, дачные бытовки, как
                        правило, производят из более дорогостоящих материалов, а соответственно они отличаются
                        презентабельным экстерьером. Недостаток – более высокая стоимость конструкции.
                        Предназначение строительной бытовки – временное размещение рабочего персонала.
                        Потому в процессе производства используются недорогие материалы и
                            минимум затрат на обустройство.</p>
                    </div>
                    <div class="item hidden" data-value="toilets">
                        <h2 class="strongest">Уличные туалеты</h2>
                    </div>
                    <div class="item hidden" data-value="burenie">
                        <h2 class="strongest">Бурение скважин</h2>
                    </div>
                    <div class="item hidden" data-value="septik">
                        <h2 class="strongest">Переливные септики</h2>

                        <p>В настоящее время существует целый ряд различных конструкций септиков, которые
                            имеют свои особенности и используют дополнительные системы. Особой популярностью
                            пользуются заводские изделия с аэратными фильтрами, электрическими насосами
                            и несколькими видами специальных бактерий.</p>
                        <p>При этом вопрос о том, как правильно сделать септик с переливом до сих пор интересуют
                            многих мастеров, поскольку подобные конструкции можно назвать самыми простыми и практичными.</p>
                    </div>

                    <div class="item hidden" data-value="communication"></div>
                    <div class="item hidden" data-value="design"></div>
                    <div class="item hidden" data-value="urist"></div>
                    <div class="item hidden" data-value="master"></div>
                    <div class="item hidden" data-value="bani"></div>
                    <div class="item hidden" data-value="zabor"></div>
                    <div class="item hidden" data-value="viezd"></div>
                    <div class="item hidden" data-value="electric"></div>
                    <div class="item hidden" data-value="santehnic"></div>
                    <div class="item hidden" data-value="otdelka"></div>
                    <div class="item hidden" data-value="mebel"></div>
                </div>
            </div>

        </div>

    </div>
</div>

    <section id="advantages">
        <div class="container text-center">
            <p class="h2 strongest mb-lg">Наши преимущества:</p>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">

                        <div class="col-md-4">
                            <img src="/img/icons/002-engineer.png" width="60" alt="">
                            <p class="h4 mt-xlg">Опытная бригада профессионалов</p>
                        </div>
                        <div class="col-md-4">
                            <p class="h1 strongest">5000+</p>
                            <p class="h4 mt-xlg">Более 5000 раз к нам обратились за нашими услугами</p>
                        </div>
                        <div class="col-md-4">
                            <img src="/img/icons/001-like.png" width="60" alt="">
                            <p class="h4 mt-xlg">Оперативно, качественно и с гарантией</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="foot">
        <div id="footer-left-stripe"></div>
        <div id="footer-right-stripe"></div>

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="promo color-white">
                        <p class="h1 text-center strongest text-uppercase">земельные работы </p>
                        <p class="description text-center"></p>

                        <p class="text-center h3 mt-xlg">Закажите прямо сейчас</p>
                        <button class="btn btn-primary btn-lg center-block mt-lg">Заказать обратный звонок</button>
                        <p class="text-center mt-lg">Или позвоните по телефону</p>
                        <p class="h3 text-center strongest mt-md ">8 (999) 956-32-21</p>
                    </div>
                </div>
            </div>
        </div>
    </section>


<footer>

    <div class="container color-white">
        <div class="row">
            <div class="col-md-3">
                <p class="h3 mt-md">Удачный сервис</p>
                <p class="h5 strongest mt-md ">8 (999) 956-32-21</p>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
            <div class="col-md-3"></div>
        </div>
    </div>
</footer>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="js/scripts.js"></script>

</body>
</html>