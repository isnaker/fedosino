<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Page Description">
    <meta name="author" content="Alex">
    <title>Page Title</title>
    <!-- Bootstrap -->
    <link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<header id="header">
    <div class="container">
        <div class="row">
            <div class="col-md-4">

            </div>
            <div class="col-md-4">
                <p class="h3 text-center mt-md">Удачный сервис</p>
            </div>
            <div class="col-md-4">
                <p class="h3 text-center lighter mt-md">8 (999) 956-32-21</p>
            </div>
        </div>
    </div>
</header>

<div id="content">
    <div class="container-fluid">
        <div class="row">
            <div id="preview">
                <h1 class="text-center strongest color-white mt-xlg text-uppercase">земельные работы </h1>
                <button class="btn btn-primary btn-lg center-block">Заказать</button>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row text-center">
            <div class="col-md-12">
                <div class="services items">
                    <div class="item">
                        <p>земельные работы (Раскопка, вспашка, выравнивание)</p>
                    </div>
                    <div class="item">
                        <p>Бытовки</p>
                    </div>
                    <div class="item">
                        <p>Уличные туалеты</p>
                    </div>
                    <div class="item">
                        <p>Бурение Скважин</p>
                    </div>
                    <div class="item">
                        <p>Переливные септики</p>
                    </div>
                    <div class="item">
                        <p>Прокладка коммуникаций по участку.</p>
                    </div>
                    <div class="item">
                        <p>Планировка участка и ландшафтный дизайн</p>
                    </div>
                    <div class="item">
                        <p>Юр. поддержка и оформление домов</p>
                    </div>
                    <div class="item">
                        <p>Мастер на час</p>
                    </div>
                    <div class="item">
                        <p>Строительство бань и летних домов</p>
                    </div>
                    <div class="item">
                        <p>Строительство заборов.</p>
                    </div>
                    <div class="item">
                        <p>Обустройство въездных групп.</p>
                    </div>
                <div class="item"><p>Электрика</p></div>
                <div class="item"><p>Сантехника</p></div>
                <div class="item"><p>Отделка помещений любой сложности</p></div>
                <div class="item"><p>Садовая мебель на заказ</p></div>

                </div>
            </div>
        </div>

    </div>
</div>

<footer>

</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

<script src="js/scripts.js"></script>

</body>
</html>