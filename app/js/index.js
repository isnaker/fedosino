$(function () {

    $('.services__list .item').click(function () {
        $('.services__list .item.current').removeClass("current");
        $(this).addClass("current");

        $('.service__content .item').addClass("hidden");
        var dataval = $(this).attr("data-target");

        $('[data-value="selected_service"]').html( $(this).find("p").html() );

        toggleServiceContent(dataval);
    })

});


function toggleServiceContent(val) {
    $('.service__content').find('[data-value="' + val + '"]').removeClass("hidden");
}